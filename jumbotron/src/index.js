const { __, setLocaleData } = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;
const { RichText, InspectorControls, MediaUpload } = wp.blockEditor;
const { Button, ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker } = wp.components;

registerBlockType( 'nmkids-bootstrap-blocks/jumbotron', {
	title: 'Jumbotron',
	icon: 'index-card',
	category: 'nmkids-bootstrap-blocks',
	attributes: {
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
		title: {
			type: 'string',
		},
		subTitle: {
			type: 'string',
		},
		backgroundColor: {
			type: 'string',
			default: '',
		},
		textColor: {
			type: 'string',
			default: 'text-muted',
		}
	},
	edit: ( props ) => {
		const {
			className,
			attributes: {
				title,
				subTitle,
				mediaID,
				mediaURL,
				textColor,
				backgroundColor,
			},
			setAttributes,
		} = props;

		const onChangeTitle = ( value ) => {
			setAttributes( { title: value } );
		};

		const onChangeSubTitle = ( value ) => {
			setAttributes( { subTitle: value } );
		};

		const onSelectImage = ( media ) => {
			setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
			} );
		};

		return (
			<div>
				<InspectorControls>
					<PanelBody
						title="Alert Settings"
						initialOpen={true}
					>
						<SelectControl
							name="backgroundColor"
							label="Background Color"
							value={backgroundColor}
							options={[
								{label: "Select", value: ""},
								{label: "Light (default)", value: ""},
								{label: "Info", value: "bg-info"},
								{label: "Primary", value: "bg-primary"},
								{label: "Secondary", value: "bg-secondary"},
								{label: "Success", value: "bg-success"},
								{label: "Warning", value: "bg-warning"},
								{label: "Danger", value: "bg-danger"},
								{label: "Dark", value: "bg-dark"},
							]}
							
							onChange={(newtext) => setAttributes({ backgroundColor: newtext })}
							
						/>
						<SelectControl
							name="textColor"
							label="Text Color"
							value={textColor}
							options={[
								{label: "Select", value: "text-muted"},
								{label: "Info", value: "text-info"},
								{label: "Primary", value: "text-primary"},
								{label: "Secondary", value: "text-secondary"},
								{label: "Success", value: "text-success"},
								{label: "Warning", value: "text-warning"},
								{label: "Danger", value: "text-danger"},
								{label: "Light", value: "text-light"},
								{label: "Dark", value: "text-dark"},
							]}
							
							onChange={(newtext) => setAttributes({ textColor: newtext })}
							
						/>

					</PanelBody>
				</InspectorControls>
				<section class={"hero py-6 py-lg-7 mb-2 " + backgroundColor}>
					<div class="container position-relative">
						<RichText
							tagName="h1"
							placeholder="Title"
							value={title}
							onChange={ onChangeTitle }
							className={"hero-heading " + textColor}
						/>
						<div class="row">
							<div class="col-xl-8 mx-auto">
								<RichText
									tagName="p"
									placeholder="Subtitle"
									multiline="br"
									value={subTitle}
									onChange={ onChangeSubTitle }
									className={textColor}
								/>
							</div>
						</div>
					</div>
				</section>
			</div>

		);
	},
	save: ( props ) => {
		const {
			className,
			attributes: {
				title,
				subTitle,
				mediaID,
				mediaURL,
				textColor,
				backgroundColor,
			},
		} = props;
		return (
			<section class={"hero py-6 py-lg-7 mb-2 " + backgroundColor}>
				<div class="container position-relative">
					<RichText.Content
						tagName="h1"
						value={title}
						className={"hero-heading " + textColor}
					/>	
					<div class="row">
						<div class="col-xl-8 mx-auto">
							<RichText.Content
								tagName="p"
								multiline="br"
								value={subTitle}
								className={textColor}
							/>	
						</div>
					</div>
				</div>
			</section>
		);
	},
} );
