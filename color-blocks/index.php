<?php

/**
 * Plugin Name: Gutenberg Examples Recipe link_block EsNext
 * Plugin URI: #
 * Description: This is a plugin demonstrating how to register new blocks for the Gutenberg editor.
 * Version: 1.0.2
 * Author: the Gutenberg Team
 *
 * @package nmkids-bootstrap-blocks
 */

defined( 'ABSPATH' ) || exit;

/**
 * Load all translations for our plugin from the MO file.
*/
add_action( 'init', 'link_block_load_textdomain' );

function link_block_load_textdomain() {
	load_plugin_textdomain( 'nmkids-bootstrap-blocks', false, basename( __DIR__ ) . '/languages' );
}

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * Passes translations to JavaScript.
 */
function nmkids_bootstrap_block_color_blocks_register_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'color-blocks',
		plugins_url( 'build/index.js', __FILE__ ),
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'underscore' ),
		filemtime( plugin_dir_path( __FILE__ ) . 'build/index.js' )
	);

	wp_register_style(
		'color-blocks',
		//get_stylesheet_directory_uri() . '/style.css',
		plugins_url( 'style.css', __FILE__ ),
		array( ),
		filemtime( plugin_dir_path( __FILE__ ) . 'style.css' )
	);

	register_block_type( 'nmkids-bootstrap-blocks/color-blocks', array(
		'style' => 'color-blocks',
		'editor_script' => 'color-blocks',
	) );

  if ( function_exists( 'wp_set_script_translations' ) ) {
    /**
     * May be extended to wp_set_script_translations( 'my-handle', 'my-domain',
     * plugin_dir_path( MY_PLUGIN ) . 'languages' ) ). For details see
     * https://make.wordpress.org/core/2018/11/09/new-javascript-i18n-support-in-wordpress/
     */
    wp_set_script_translations( 'link_block', 'nmkids-bootstrap-blocks' );
  }

}
add_action( 'init', 'nmkids_bootstrap_block_color_blocks_register_block' );
