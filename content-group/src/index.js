import { InnerBlocks } from '@wordpress/block-editor';
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls, MediaUpload } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker, Button, } = wp.components;
 
registerBlockType('nmkids-bootstrap-blocks/content-group', {
	title: 'Content Group Component',
	category: 'nmkids-bootstrap-blocks',
	icon: 'warning',
	keywords: ['example', 'test'],
	attributes: {
		mediaTitle: {
			type: 'string',
		},
		mediaBody: {
			type: 'string',
		},
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
	},
	edit: (props) => { 
		const {
			className,
			attributes: {
				mediaTitle,
				mediaBody,
				mediaID,
				mediaURL,
			},
			setAttributes,
		} = props;
		
		const onSelectImage = ( media ) => {
			setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
			} );
		};

		const onChangeTitle = ( value ) => {
			setAttributes( { mediaTitle: value } );
		};

		const onChangeBody = ( value ) => {
			setAttributes( { mediaBody: value } );
		};

		return (
			<div className="media mb-3">
				<div className="mr-3">
					<MediaUpload
						onSelect={ onSelectImage }
						allowedTypes="image"
						value={ mediaID }
						render={ ( { open } ) => (
							<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
								{ ! mediaID ? ( 'Upload Image', 'content-group' ) : <img src={ mediaURL } alt={ ( 'Upload Image', 'content-group' ) } /> }
							</Button>
						) }
					/>
				</div>
				<div className="media-body">
					<RichText 
						tagName="h4"
						placeholder="Title"
						value={mediaTitle}
						onChange={ onChangeTitle }
						className="mt-0"
					/>
					<RichText
						tagName="p"
						placeholder="Write your paragraph here"
						value={mediaBody}
						onChange={ onChangeBody }
					/>
					<InnerBlocks />
				</div>
			</div>
		);
	},
	save: (props) => { 
		const {
			className,
			attributes: {
				mediaTitle,
				mediaBody,
				mediaURL,
			},
		} = props;
		return (
			<div className="media mb-3">
				{
					mediaURL && (
						<img className="mr-3" src={ mediaURL } />
					)
				}
				<div class="media-body">
					<RichText.Content 
						tagName="h5"
						value={mediaTitle}
						className="mt-0 mb-0"
					/>
					<RichText.Content 
						tagName="p"
						value={mediaBody}
						className="text-muted mb-3"
					/>
					<InnerBlocks.Content />
				</div>
			</div>
		);
	}
});