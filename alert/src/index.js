const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker } = wp.components;
 
registerBlockType('nmkids-bootstrap-blocks/alert', {
	title: 'Alert Component',
	category: 'nmkids-bootstrap-blocks',
	icon: 'warning',
	keywords: ['example', 'test'],
	attributes: {
		myRichHeading: {
			type: 'string',
		},
		myRichText: {
			type: 'string',
			source: 'html',
			selector: 'p'
		},
		toggle: {
			type: 'boolean',
			default: false
		},
		alertClass: {
			type: 'string',
			default: 'bd-callout bd-callout-'
		},
		alertColor: {
			type: 'string',
			default: 'info',
		}
	},
	edit: (props) => { 
		const { className, attributes, setAttributes } = props;

		var cssClass = '';

		var titleClass = '';

		function cssAlertType(alert_type) {
			if(alert_type.toggle == true) {
				var alert_text = 'alert alert-';
				setAttributes({ alertClass: 'alert alert-'});
				console.log(alert_text);
			}
			else {
				console.log('bd-callout bd-callout-');
				setAttributes({ alertClass: 'bd-callout bd-callout-'});
				console.log(alert_text);
			}
			//console.log(alert_type.);
			//return alert_type = alert_type
		}
		
		return (
			<div>
				<InspectorControls>
					<PanelBody
						title="Alert Settings"
						initialOpen={true}
					>
						<ToggleControl
							label="Full-Color Alert"
							value="alert alert-"
							checked={attributes.toggle}
							//onChange={(newval) => setAttributes({ toggle: newval })}
							onChange={(newval) => { setAttributes({ toggle: newval}); cssAlertType({ toggle: newval}) }}
						/>
						<SelectControl
							name="alertColor"
							label="Alert Color"
							value={attributes.alertTitleClass}
							options={[
								{label: "Info", value: "info"},
								{label: "Primary", value: "primary"},
								{label: "Secondary", value: "secondary"},
								{label: "Success", value: "success"},
								{label: "Warning", value: "warning"},
								{label: "Danger", value: "danger"},
								//{label: "Light", value: "light"},
								{label: "Dark", value: "dark"},
							]}
							//onChange={(colorVal) => generateAlertClass( alertName.value, { alertTitleClass: colorVal } )}
							onChange={(newtext) => setAttributes({ alertColor: newtext })}
							//onChange={(newval) => cssAlertColor( { alertTitleClass: newval } )}
						/>
					</PanelBody>
				</InspectorControls>
				<div className={attributes.alertClass + attributes.alertColor}>
					<RichText 
						tagName="h4"
						placeholder="Write your heading here"
						value={attributes.myRichHeading}
						onChange={(newtext) => setAttributes({ myRichHeading: newtext })}
						className={'text-' + attributes.alertColor}
					/>
					<RichText
						tagName="p"
						placeholder="Write your paragraph here"
						value={attributes.myRichText}
						onChange={(newtext) => setAttributes({ myRichText: newtext })}
					/>
				</div>
			</div>
		);
	},
	save: (props) => { 
		const { attributes } = props;
		return (
			<div class={attributes.alertClass + attributes.alertColor}>
				<RichText.Content 
					tagName="h4"
					value={attributes.myRichHeading}
					className={'alert-title text-' + attributes.alertColor}
				/>
				<RichText.Content 
					tagName="p"
					value={attributes.myRichText}
					className="alert-body"
				/>
			</div>
		);
	}
});