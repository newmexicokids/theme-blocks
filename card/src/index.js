const { __, setLocaleData } = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;
const {
	RichText,
	MediaUpload,
} = wp.editor;
const { Button } = wp.components;

registerBlockType( 'nmkids-bootstrap-blocks/card', {
	title: 'Card',
	icon: 'index-card',
	category: 'nmkids-bootstrap-blocks',
	attributes: {
		mediaID: {
			type: 'number',
		},
		mediaURL: {
			type: 'string',
			source: 'attribute',
			selector: 'img',
			attribute: 'src',
		},
		title: {
			type: 'array',
			source: 'children',
			selector: '.card-title',
		},
		body: {
			type: 'array',
			source: 'children',
			selector: '.card-content',
		},
		action: {
			type: 'array',
			source: 'children',
			selector: '.card-footer'
		}
	},
	edit: ( props ) => {
		const {
			className,
			attributes: {
				title,
				mediaID,
				mediaURL,
				body,
			},
			setAttributes,
		} = props;
		const onChangeTitle = ( value ) => {
			setAttributes( { title: value } );
		};

		const onSelectImage = ( media ) => {
			setAttributes( {
				mediaURL: media.url,
				mediaID: media.id,
			} );
		};
		const onChangeBody = ( value ) => {
			setAttributes( { body: value } );
		};

		return (
				<div class="card border-0 shadow mb-3">
					<div className="card-img-top card-responsive" height="150px">
						<MediaUpload
							onSelect={ onSelectImage }
							allowedTypes="image"
							value={ mediaID }
							render={ ( { open } ) => (
								<Button className={ mediaID ? 'image-button' : 'button button-large' } onClick={ open }>
									{ ! mediaID ? __( 'Upload Image', 'gutenberg-examples' ) : <img src={ mediaURL } alt={ __( 'Upload Card Image (optional)', 'nmkids-bootstrap-blocks' ) } /> }
								</Button>
							) }
						/>
					</div>
					<div class="card-body">
						<RichText
							tagName="h2"
							className="card-title"
							placeholder={ __( 'Card Title...', 'nmkids-bootstrap-blocks' ) }
							value={ title }
							onChange={ onChangeTitle }
						/>
						<RichText
							tagName="div"
							multiline="p"
							className="card-content"
							placeholder={ __( 'Card Content...', 'gutenberg-examples' ) }
							value={ body }
							onChange={ onChangeBody }
						/>
					</div>
				</div>

		);
	},
	save: ( props ) => {
		const {
			className,
			attributes: {
				title,
				mediaID,
				mediaURL,
				body,
			},
		} = props;
		return (
				<div class="card border-0 shadow mb-3">
					{
						mediaURL && (
							<img className="card-img-top" src={ mediaURL } alt={ __( 'Card Image', 'nmkids-bootstrap-blocks' ) } />
						)
					}
					<div class="card-body">
						<RichText.Content tagName="h4" className="card-title" value={ title } />
						<RichText.Content tagName="div" className="card-content" value={ body } />
					</div>
				</div>
		);
	},
} );
