const { __, setLocaleData } = wp.i18n;
const {
	registerBlockType,
	registerFormatType,
	richTextToolbarButton,
} = wp.blocks;
const { RichText, InspectorControls, MediaUpload, } = wp.blockEditor;
const { Button, ToggleControl, TextControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker } = wp.components;

registerBlockType( 'nmkids-bootstrap-blocks/color-blocks', {
	title: 'Block Links',
	icon: 'grid-view',
	category: 'nmkids-bootstrap-blocks',
	attributes: {
		linkOneTitle: {
			type: 'string',
		},
		linkOneAnchor: {
			type: 'string',
			attribute: 'href',
		},
		linkOneIcon: {
			type: 'string',
		},
		linkTwoTitle: {
			type: 'string',
		},
		linkTwoAnchor: {
			type: 'string',
			attribute: 'href',
		},
		linkTwoIcon: {
			type: 'string',
		},
		linkThreeTitle: {
			type: 'string',
		},
		linkThreeAnchor: {
			type: 'string',
			attribute: 'href',
		},
		linkThreeIcon: {
			type: 'string',
		},
		linkFourTitle: {
			type: 'string',
		},
		linkFourAnchor: {
			type: 'string',
			attribute: 'href',
		},
		linkFourIcon: {
			type: 'string',
		},
	},
	edit: ( props ) => {
		const {
			className,
			attributes: {
				linkOneTitle,
				linkTwoTitle,
				linkThreeTitle,
				linkFourTitle,
				linkOneAnchor,
				linkTwoAnchor,
				linkThreeAnchor,
				linkFourAnchor,
			},
			setAttributes,
		} = props;

		const onChangeTitleOne = ( value ) => {
			setAttributes( { linkOneTitle: value } );
			//setAttributes( { linkOneTitle: value } );
		};

		const onChangeTitleTwo = ( value ) => {
			setAttributes( { linkTwoTitle: value } );
		};

		const onChangeTitleThree = ( value ) => {
			setAttributes( { linkThreeTitle: value } );
		};

		const onChangeTitleFour = ( value ) => {
			setAttributes( { linkFourTitle: value } );
		};

		const onChangeAnchorOne = ( value ) => {
			setAttributes( { linkOneAnchor: value } );
		};

		const onChangeAnchorTwo = ( value ) => {
			setAttributes( { linkTwoAnchor: value } );
		};

		const onChangeAnchorThree = ( value ) => {
			setAttributes( { linkThreeAnchor: value } );
		};

		const onChangeAnchorFour = ( value ) => {
			setAttributes( { linkFourAnchor: value } );
		};

		return (
			<div>
				<InspectorControls>
					<PanelBody
						title="Link One"
						initialOpen={false}
					>
						<PanelRow>
							<TextControl
								label="Anchor / URL"
								placeholder="https://www.newmexicokids.org/"
								value={ linkOneAnchor }
								onChange={ onChangeAnchorOne }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody
						title="Link Two"
						initialOpen={false}
					>
						<PanelRow>
							<TextControl
								label="Anchor / URL"
								placeholder="https://www.newmexicokids.org/"
								value={ linkTwoAnchor }
								onChange={ onChangeAnchorTwo }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody
						title="Link Three"
						initialOpen={false}
					>
						<PanelRow>
							<TextControl
								label="Anchor / URL"
								placeholder="https://www.newmexicokids.org/"
								value={ linkThreeAnchor }
								onChange={ onChangeAnchorThree }
							/>
						</PanelRow>
					</PanelBody>
					<PanelBody
						title="Link Four"
						initialOpen={false}
					>
						<PanelRow>
							<TextControl
								label="Anchor / URL"
								placeholder="https://www.newmexicokids.org/"
								value={ linkFourAnchor }
								onChange={ onChangeAnchorFour }
							/>
						</PanelRow>
					</PanelBody>
				</InspectorControls>
				<div class="container-fluid full-width">
					<div class="row">
						<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#c05131'}}>
							<a href={linkOneAnchor} class="d-block">
								<div class="px-0 px-lg-3">
									<RichText
										tagName="h3"
										className="h5 text-light"
										placeholder="Link Title"
										name="linkOne"
										value={ linkOneTitle }
										onChange={ onChangeTitleOne }
									/>
								</div>
							</a>
						</div>
						<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#8a387c'}}>
							<a href={linkTwoAnchor} class="d-block">
								<div class="px-0 px-lg-3">
									<RichText
										tagName="h3"
										className="h5 text-light"
										placeholder="Link Title"
										name="linkTwo"
										value={ linkTwoTitle }
										//onChange={ ( value ) => onChangeTitleTwo( { value } ) }
										onChange={ onChangeTitleTwo }
									/>
								</div>
							</a>
						</div>
						<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#007a86'}}>
							<a href={linkThreeAnchor} class="d-block">
								<div class="px-0 px-lg-3">
									<RichText
										tagName="h3"
										className="h5 text-light"
										placeholder="Link Title"
										name="linkThree"
										value={ linkThreeTitle }
										//onChange={ ( value ) => onChangeTitleThree( { value } ) }
										onChange={ onChangeTitleThree }
									/>
								</div>
							</a>
						</div>
						<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#a8aa19'}}>
							<a href={linkFourAnchor} class="d-block">
								<div class="px-0 px-lg-3">
									<RichText
										tagName="h3"
										className="h5 text-light"
										placeholder="Link Title"
										name="linkFour"
										value={ linkFourTitle }
										//onChange={ ( value ) => onChangeTitleFour( { value } ) }
										onChange={ onChangeTitleFour }
									/>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>

		);
	},
	save: ( props ) => {
		const {
			className,
			attributes: {
				linkOneTitle,
				linkTwoTitle,
				linkThreeTitle,
				linkFourTitle,
				linkOneAnchor,
				linkTwoAnchor,
				linkThreeAnchor,
				linkFourAnchor,
			},
		} = props;
		return (
			<div class="container-fluid full-width">
				<div class="row">
					<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#c05131'}}>
						<a href={ linkOneAnchor } class="d-block">
							<div class="px-0 px-lg-3">
								<RichText.Content tagName="h3" className="h5 text-light" value={ linkOneTitle } />
							</div>
						</a>
					</div>
					<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#8a387c'}}>
						<a href={ linkTwoAnchor } class="d-block">
							<div class="px-0 px-lg-3">
								<RichText.Content tagName="h3" className="h5 text-light" value={ linkTwoTitle } />
							</div>
						</a>
					</div>
					<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#007a86'}}>
						<a href={ linkThreeAnchor } class="d-block">
							<div class="px-0 px-lg-3">
								<RichText.Content tagName="h3" className="h5 text-light" value={ linkThreeTitle } />
							</div>
						</a>
					</div>
					<div class="col-md-3 py-5 text-center" style={{backgroundColor: '#a8aa19'}}>
						<a href={ linkFourAnchor } class="d-block">
							<div class="px-0 px-lg-3">
								<RichText.Content tagName="h3" className="h5 text-light" value={ linkFourTitle } />
							</div>
						</a>
					</div>
				</div>
			</div>
		);
	},
} );
