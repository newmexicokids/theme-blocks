const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker } = wp.components;
 
registerBlockType('nmkids-bootstrap-blocks/badge', {
	title: 'Badge Component',
	category: 'nmkids-bootstrap-blocks',
	icon: 'tag',
	keywords: ['example', 'test'],
	attributes: {
		content: {
			type: 'string',
		},
		alertColor: {
			type: 'string',
			default: 'info',
		}
	},
	edit: (props) => { 
		const { className, attributes, setAttributes } = props;

		return (
			<div>
				<InspectorControls>
					<PanelBody
						title="Badge Settings"
						initialOpen={true}
					>
						<SelectControl
							name="alertColor"
							label="Badge Color"
							value={attributes.alertTitleClass}
							options={[
								{label: "Info", value: "badge badge-info"},
								{label: "Primary", value: "badge badge-primary"},
								{label: "Secondary", value: "badge badge-secondary"},
								{label: "Success", value: "badge badge-success"},
								{label: "Warning", value: "badge badge-warning"},
								{label: "Danger", value: "badge badge-danger"},
								//{label: "Light", value: "light"},
								{label: "Dark", value: "badge badge-dark"},
							]}
							//onChange={(colorVal) => generateAlertClass( alertName.value, { alertTitleClass: colorVal } )}
							onChange={(newtext) => setAttributes({ alertColor: newtext })}
							//onChange={(newval) => cssAlertColor( { alertTitleClass: newval } )}
						/>
					</PanelBody>
				</InspectorControls>
				<RichText
					tagName="span"
					className={attributes.alertColor}
					placeholder="text here..."
					value={attributes.content}
					onChange={(newtext) => setAttributes({ content: newtext })}
				/>
			</div>
		);
	},
	save: (props) => { 
		const { attributes } = props;
		return (
			
			<RichText.Content 
				tagName="span"
				value={attributes.content}
				className={attributes.alertColor}
			/>
		);
	}
});