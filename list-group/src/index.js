const { __, setLocaleData } = wp.i18n;
const {
	registerBlockType,
} = wp.blocks;
const {
	RichText,
	MediaUpload,
} = wp.editor;
const { Button } = wp.components;

registerBlockType( 'nmkids-bootstrap-blocks/list-group', {
	title: 'List Group',
	icon: 'index-card',
	category: 'NMKids Bootstrap Blocks',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		ingredients: {
			type: 'array',
			source: 'children',
			selector: '.list-group',
		},
	},
	edit: ( props ) => {
		const {
			className,
			attributes: {
				title,
				ingredients,
			},
			setAttributes,
		} = props;

		const onChangeTitle = ( value ) => {
			setAttributes( { title: value } );
		};

		const onChangeIngredients = ( value ) => {
			setAttributes( { ingredients: value } );
		};

		return (
			<div class="col-12">
				<RichText
					tagName="h2"
					placeholder={ __( 'Write List Title…', 'gutenberg-examples' ) }
					value={ title }
					onChange={ onChangeTitle }
				/>
				<h3>{ __( 'Ingredients', 'gutenberg-examples' ) }</h3>
				<RichText
					tagName="ul"
					multiline="li"
					placeholder={ __( 'Write a list of ingredients…', 'gutenberg-examples' ) }
					value={ ingredients }
					onChange={ onChangeIngredients }
					className="list-group list-group-items"
				/>
			</div>
		);
	},
	save: ( props ) => {
		const {
			className,
			attributes: {
				title,
				ingredients,
			},
		} = props;
		return (
			<div className={ className }>
				<RichText.Content tagName="h2" value={ title } />

				
					<RichText.Content tagName="ul" value={ ingredients } />
				
				
			</div>
		);
	},
} );
