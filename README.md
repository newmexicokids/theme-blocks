# NMKids Theme Blocks #
**Requires at least Wordpress:** 5.8  
**Tested up to:** 5.8.1  
**Stable tag:** 1.2  
**Requires PHP:** 7.3  
**License:** UNM Early Childhood Services Center, All Rights Reserved  
**License URI:** https://www.gnu.org/licenses/gpl-2.0.html  

Custom blocks for the NewMexicoKids.org theme. Required for the NMKids theme. Does not work with other themes.

## Description ##

This plugin is a custom plugin developed by the Resource and Referral Web Team at the University of New Mexico Early Childhood Services Center.

The plugin works with the custom Wordpress Theme NMKids, developed internally by the Resource and Referral Web Team.

This plugin does **NOT** work with other themes, and is designed specifically to work with the NMKids theme. It is a required plugin of the theme.

Any questions, comments, or issues found with this theme can be directed to support@newmexicokids.org through the ticket system, or to the plugin developers at Resource and Referral.

To make changes to the readme.txt in Markdown format for Bitbucket (README.md), open this .txt file in Sublime, and press `Command + Control + k`

## Changelog ##

### 1.2.0 ###
* Set up Bitbucket Development Workflow
* Attempts to fix bitbucket download issue
* changes minimum required version for WP

### 1.1.b ###
* Fixes issue with auto updates and using wordpress default updater

### 1.1.a ###
* fixes issue with links in color-blocks
* adds support for auto updates
* removes redundant code and old / unstable versions

### 1.1.0 ###
* adds custom color-blocks and updates linking abilities
* fixes issue with display of media in cards
* fixes issue with jQuery compatibility of Wordpress and Theme for Collapse Elements

### 1.0 ###
* Initial release
* includes the following bootstrap components:
	* Cards
	* Media Group (content-group)
	* Jumbotron
	* Collapse Elements
	* Badges
	* Alerts

## Upgrade Notice ##

### 1.0 ###
Initial release, semi-stable. Upgrade to v1.1.0 immedietly

## Development Notes ##
In this section, write the notes for the branch you are developing below this line

* created large buttons feature branch and began work.
