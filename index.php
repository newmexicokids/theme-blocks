<?php

/*
  Plugin Name: NMKids Theme Blocks
  Version: 1.2.0
  Description: Custom blocks for the NewMexicoKids.org theme. Required for the NMKids theme.
  Author: Garrett Massey
  Author URI: https://www.garrettmassey.net/
  License: Copyright (c) 2021, University of New Mexico Early Childhood Services Center
 */

defined( 'ABSPATH' ) || exit;

function nmkids_bootstrap_block_categories( $categories ) {
    return array_merge(
        $categories,
        [
            [
                'slug'  => 'nmkids-bootstrap-blocks',
                'title' => __( 'NMKids Bootstrap Blocks', 'nmkids-boilerplate' ),
            ],
        ]
    );
}
add_action( 'block_categories_all', 'nmkids_bootstrap_block_categories', 10, 2 );

include 'card/index.php';
include 'alert/index.php';
include 'content-group/index.php';
include 'badge/index.php';
include 'collapse/index.php';
include 'jumbotron/index.php';
include 'color-blocks/index.php';

/* Plugin Update Checker */
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/newmexicokids/theme-blocks/',
    __FILE__,
    'unique-plugin-or-theme-slug'
);

$myUpdateChecker->setAuthentication(array(
    'consumer_key' => 'EZ3t3Vd2rWSXNMj84z',
    'consumer_secret' => 'NruegZHRN2Le2fKNap7mTrxNWSSyAEbG',
));

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

