import { InnerBlocks } from '@wordpress/block-editor';
const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls } = wp.blockEditor;
const { ToggleControl, PanelBody, PanelRow, CheckboxControl, SelectControl, ColorPicker } = wp.components;
 
registerBlockType('nmkids-bootstrap-blocks/collapse', {
	title: 'Collapse',
	category: 'nmkids-bootstrap-blocks',
	icon: 'feedback',
	keywords: ['example', 'test'],
	attributes: {
		collapseTitle: {
			type: 'string',
		},
		collapseBody: {
			type: 'string',
		},
		headingID: {
			type: 'string',
			//default: 'test',
		},
		collapseID: {
			type: 'string',
			//default: 'test',
		},
		parentID: {
			type: 'string',
		}
	},
	edit: (props) => { 
		const {
			className,
			attributes: {
				collapseTitle,
				collapseBody,
				headingID,
				collapseID,
				parentID,
			},
			setAttributes,
		} = props;
		
		// util function to convert the input to string type
		function convertToString(input) {
		
			if(input) { 
			  
				  if(typeof input === "string") {
	 
						 return input;
					}
				 
				  return String(input);
			}
			return '';
		}


		// convert string to words
		function toWords(input) {             
		  
			input = convertToString(input);
			
			var regex = /[A-Z\xC0-\xD6\xD8-\xDE]?[a-z\xDF-\xF6\xF8-\xFF]+|[A-Z\xC0-\xD6\xD8-\xDE]+(?![a-z\xDF-\xF6\xF8-\xFF])|\d+/g;
			
			return toCamelCase(input.match(regex));
		  
		}

		function toCamelCase(inputArray) {

				let result = "";

				for(let i = 0 , len = inputArray.length; i < len; i++) {

					let currentStr = inputArray[i];
			
					let tempStr = currentStr.toLowerCase();

					if(i != 0) {
			
						// convert first letter to upper case (the word is in lowercase) 
							tempStr = tempStr.substr(0, 1).toUpperCase() + tempStr.substr(1);

					 }
					
					 result +=tempStr;
					
				}
			
				return result;
		}

		const onChangeTitle = ( value ) => {
			setAttributes( { collapseTitle: value } );
			setAttributes( { headingID: toWords(value) });
			setAttributes( { collapseID: toWords(value) });
		};

		const onChangeBody = ( value ) => {
			setAttributes( { collapseBody: value } );
			
		};
		
		return (
			<div id={"accordion" + collapseID}>
				<div class="mb-3">
					<div class="card-header shadow bg-primary-100 border-0 py-0" id={"heading" + headingID} role="tab">
						<RichText
							tagName="a"
							className="accordion-link collapsed"
							data-toggle="collapse"
							placeholder="Collapse Group Title..."
							href={'#' + collapseID}
							aria-expanded="true"
							aria-controls={ collapseID }
							value={ collapseTitle }
							onChange={ onChangeTitle }
						/>
					</div>
					<div class="collapse show" id={collapseID} role="tabpanel" aria-labelledby={"heading" + headingID} data-parent={"#accordion" + collapseID}>
						<div class="px-3 pt-3">
							<RichText
								tagName="div"
								multiline="p"
								className="text-muted pb-1"
								placeholder="Collapse Content... or..."
								value={collapseBody}
								onChange={ onChangeBody }
							/>
							<InnerBlocks />
						</div>
					</div>
				</div>
			</div>
		);
	},
	save: (props) => { 
		const {
			className,
			attributes: {
				collapseTitle,
				collapseBody,
				collapseID,
				headingID,
			},
		} = props;
		return (
			<div id={"accordion" + collapseID}>
				<div class="mb-3">
					<div class="card-header shadow bg-primary-100 border-0 py-0" id={"heading" + headingID} role="tab">
						<RichText.Content 
							tagName="a"
							className="accordion-link collapsed"
							data-toggle="collapse"
							href={'#' + collapseID}
							aria-expanded="false"
							aria-controls={ collapseID }
							value={ collapseTitle }
						/>
					</div>
					<div class="collapse" id={collapseID} role="tabpanel" aria-labelledby={"heading" + headingID} data-parent={"#accordion" + collapseID}>
						<div class="px-3 pt-3">
							<RichText.Content 
								tagName="div"
								className="text-muted pb-1"
								value={collapseBody}
							/>
							<InnerBlocks.Content />
						</div>
					</div>
				</div>
			</div>
		);
	}
});