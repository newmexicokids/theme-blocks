/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);

var _wp$i18n = wp.i18n,
    __ = _wp$i18n.__,
    setLocaleData = _wp$i18n.setLocaleData;
var _wp$blocks = wp.blocks,
    registerBlockType = _wp$blocks.registerBlockType,
    registerFormatType = _wp$blocks.registerFormatType,
    richTextToolbarButton = _wp$blocks.richTextToolbarButton;
var _wp$blockEditor = wp.blockEditor,
    RichText = _wp$blockEditor.RichText,
    InspectorControls = _wp$blockEditor.InspectorControls,
    MediaUpload = _wp$blockEditor.MediaUpload;
var _wp$components = wp.components,
    Button = _wp$components.Button,
    ToggleControl = _wp$components.ToggleControl,
    TextControl = _wp$components.TextControl,
    PanelBody = _wp$components.PanelBody,
    PanelRow = _wp$components.PanelRow,
    CheckboxControl = _wp$components.CheckboxControl,
    SelectControl = _wp$components.SelectControl,
    ColorPicker = _wp$components.ColorPicker;
registerBlockType('nmkids-bootstrap-blocks/color-blocks', {
  title: 'Block Links',
  icon: 'grid-view',
  category: 'nmkids-bootstrap-blocks',
  attributes: {
    linkOneTitle: {
      type: 'string'
    },
    linkOneAnchor: {
      type: 'string',
      attribute: 'href'
    },
    linkOneIcon: {
      type: 'string'
    },
    linkTwoTitle: {
      type: 'string'
    },
    linkTwoAnchor: {
      type: 'string',
      attribute: 'href'
    },
    linkTwoIcon: {
      type: 'string'
    },
    linkThreeTitle: {
      type: 'string'
    },
    linkThreeAnchor: {
      type: 'string',
      attribute: 'href'
    },
    linkThreeIcon: {
      type: 'string'
    },
    linkFourTitle: {
      type: 'string'
    },
    linkFourAnchor: {
      type: 'string',
      attribute: 'href'
    },
    linkFourIcon: {
      type: 'string'
    }
  },
  edit: function edit(props) {
    var className = props.className,
        _props$attributes = props.attributes,
        linkOneTitle = _props$attributes.linkOneTitle,
        linkTwoTitle = _props$attributes.linkTwoTitle,
        linkThreeTitle = _props$attributes.linkThreeTitle,
        linkFourTitle = _props$attributes.linkFourTitle,
        linkOneAnchor = _props$attributes.linkOneAnchor,
        linkTwoAnchor = _props$attributes.linkTwoAnchor,
        linkThreeAnchor = _props$attributes.linkThreeAnchor,
        linkFourAnchor = _props$attributes.linkFourAnchor,
        setAttributes = props.setAttributes;

    var onChangeTitleOne = function onChangeTitleOne(value) {
      setAttributes({
        linkOneTitle: value
      }); //setAttributes( { linkOneTitle: value } );
    };

    var onChangeTitleTwo = function onChangeTitleTwo(value) {
      setAttributes({
        linkTwoTitle: value
      });
    };

    var onChangeTitleThree = function onChangeTitleThree(value) {
      setAttributes({
        linkThreeTitle: value
      });
    };

    var onChangeTitleFour = function onChangeTitleFour(value) {
      setAttributes({
        linkFourTitle: value
      });
    };

    var onChangeAnchorOne = function onChangeAnchorOne(value) {
      setAttributes({
        linkOneAnchor: value
      });
    };

    var onChangeAnchorTwo = function onChangeAnchorTwo(value) {
      setAttributes({
        linkTwoAnchor: value
      });
    };

    var onChangeAnchorThree = function onChangeAnchorThree(value) {
      setAttributes({
        linkThreeAnchor: value
      });
    };

    var onChangeAnchorFour = function onChangeAnchorFour(value) {
      setAttributes({
        linkFourAnchor: value
      });
    };

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(InspectorControls, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: "Link One",
      initialOpen: false
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(TextControl, {
      label: "Anchor / URL",
      placeholder: "https://www.newmexicokids.org/",
      value: linkOneAnchor,
      onChange: onChangeAnchorOne
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: "Link Two",
      initialOpen: false
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(TextControl, {
      label: "Anchor / URL",
      placeholder: "https://www.newmexicokids.org/",
      value: linkTwoAnchor,
      onChange: onChangeAnchorTwo
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: "Link Three",
      initialOpen: false
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(TextControl, {
      label: "Anchor / URL",
      placeholder: "https://www.newmexicokids.org/",
      value: linkThreeAnchor,
      onChange: onChangeAnchorThree
    }))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelBody, {
      title: "Link Four",
      initialOpen: false
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(PanelRow, null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(TextControl, {
      label: "Anchor / URL",
      placeholder: "https://www.newmexicokids.org/",
      value: linkFourAnchor,
      onChange: onChangeAnchorFour
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "container-fluid full-width"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "row"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#c05131'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkOneAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText, {
      tagName: "h3",
      className: "h5 text-light",
      placeholder: "Link Title",
      name: "linkOne",
      value: linkOneTitle,
      onChange: onChangeTitleOne
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#8a387c'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkTwoAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText, {
      tagName: "h3",
      className: "h5 text-light",
      placeholder: "Link Title",
      name: "linkTwo",
      value: linkTwoTitle //onChange={ ( value ) => onChangeTitleTwo( { value } ) }
      ,
      onChange: onChangeTitleTwo
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#007a86'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkThreeAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText, {
      tagName: "h3",
      className: "h5 text-light",
      placeholder: "Link Title",
      name: "linkThree",
      value: linkThreeTitle //onChange={ ( value ) => onChangeTitleThree( { value } ) }
      ,
      onChange: onChangeTitleThree
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#a8aa19'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkFourAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText, {
      tagName: "h3",
      className: "h5 text-light",
      placeholder: "Link Title",
      name: "linkFour",
      value: linkFourTitle //onChange={ ( value ) => onChangeTitleFour( { value } ) }
      ,
      onChange: onChangeTitleFour
    })))))));
  },
  save: function save(props) {
    var className = props.className,
        _props$attributes2 = props.attributes,
        linkOneTitle = _props$attributes2.linkOneTitle,
        linkTwoTitle = _props$attributes2.linkTwoTitle,
        linkThreeTitle = _props$attributes2.linkThreeTitle,
        linkFourTitle = _props$attributes2.linkFourTitle,
        linkOneAnchor = _props$attributes2.linkOneAnchor,
        linkTwoAnchor = _props$attributes2.linkTwoAnchor,
        linkThreeAnchor = _props$attributes2.linkThreeAnchor,
        linkFourAnchor = _props$attributes2.linkFourAnchor;
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "container-fluid full-width"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "row"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#c05131'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkOneAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText.Content, {
      tagName: "h3",
      className: "h5 text-light",
      value: linkOneTitle
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#8a387c'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkTwoAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText.Content, {
      tagName: "h3",
      className: "h5 text-light",
      value: linkTwoTitle
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#007a86'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkThreeAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText.Content, {
      tagName: "h3",
      className: "h5 text-light",
      value: linkThreeTitle
    })))), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "col-md-3 py-5 text-center",
      style: {
        backgroundColor: '#a8aa19'
      }
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("a", {
      href: linkFourAnchor,
      class: "d-block"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      class: "px-0 px-lg-3"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(RichText.Content, {
      tagName: "h3",
      className: "h5 text-light",
      value: linkFourTitle
    }))))));
  }
});

/***/ }),

/***/ "@wordpress/element":
/*!*********************************!*\
  !*** external ["wp","element"] ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = window["wp"]["element"]; }());

/***/ })

/******/ });
//# sourceMappingURL=index.js.map